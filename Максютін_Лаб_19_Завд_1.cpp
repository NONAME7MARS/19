#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include<bits/stdc++.h>
using namespace std;
//����� ������
struct node
{
	int x;
	node *l;
	node *r;
};
// ��������� �� ����� �������� ������ � ���� ���������

void show_childr(node *&mt)
{
	node *cl, *cr;
	if (mt != NULL)   //�������� �� �������� �����
		{
			cout << " ����� " << " "; //��������� �������� � ��������� ����
			cout << " �������� ����� = " << mt->x << " "; //��������� �������� � ��������� ����
			if ((mt->l)!=NULL)   //�������� �� �������� �����
				{
					cl= mt->l;
					cout << " ˳��� ������� = " << cl->x << " ";
				}
			if ((mt->r)!=NULL)
				{
					cr= mt->r;
					cout << " ������ ������� = " << cr->x << " ";
				}
			cout << ";\n";
			show_childr(mt->l); //������ ���������� ������� ��� �������� �� ����� �����
			show_childr(mt->r); //������ ���������� ������� ��� �������� �� ������� �����
		}
}
// ���������� ������� ��� ��������� ����� ������
int count(node *&mt)
{
	if (mt == NULL) //�������� �� �������� �����
		return 0;
	return 1+count(mt->l)+count(mt->r);
}
// �������� ������
void del(node *&mt)
{
	if (mt != NULL)   //�������� �� �������� �����
		{
			del(mt->l); //������ ���������� ������� ��� �������� �� ����� �����
			del(mt->r); //������ ���������� ������� ��� �������� �� ������� �����
			delete mt; //��������� �����
		}
	mt=NULL;
}
// ������ ����� � ������������ ������
void add_node_balans(int x, node *&mt)
{
	if (NULL == mt)
		{
			mt = new node;
			mt->x = x;
			mt->l = NULL;
			mt->r = NULL;
		}
	else
		{
			if (count(mt->l)<count(mt->r))
				{
					if (mt->l != NULL)
						add_node_balans(x, mt->l); //������ ���������� ������� ��� �������� �� ����� �����
					else
						{
							mt->l = new node;
							mt->l->l = NULL;
							mt->l->r = NULL;
							mt->l->x = x;
						}
				}
			else
				{
					if (mt->r != NULL)
						add_node_balans(x, mt->r); //������ ���������� ������� ��� �������� �� ������� �����
					else
						{
							mt->r = new node;
							mt->r->l = NULL;
							mt->r->r = NULL;
							mt->r->x = x;
						}
				}
		}
}


//�������� �� � ����� ������� � �����
bool find_node(node *&mt, int e)
{
	int q=0;
	if (mt != NULL)
		{
			q=find_node(mt->l, e);
			if (mt->x == e)
				return 1;
			q=q+find_node(mt->r, e);
			return q;
		}
	else
		return 0;
}
void findAmountOfParents(node* mt, int num, int amount, int& len)
{
	if (mt == NULL)
		{
			return;
		}
	if (num == mt->x)
		{
			len = amount;
			return;
		}
	findAmountOfParents(mt->l, num, amount + 1, len);
	findAmountOfParents(mt->r, num, amount + 1, len);
}
int main()
{
	const int n=15;
	int i, e;
	int len = -1;
	int el[n]= {};
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout<<"������ ����� ��� ��������� ������:";
	for(i=0;i<n;i++)
	{
		cin>>el[i];
	}
	node *tree = NULL;
	cout << "\n������ ������������ ������ "<<"\n";
	i=0;
	while (i<n)
		{
			add_node_balans(el[i], tree);
			i++;
		}
	cout << "\nʳ������ ����� ������: " <<count(tree)<<"\n";
	cout << "\n����������� ����� ������: \n";
	show_childr(tree);
	cout << "\n";
	cout << "\n������ �������, ���� ��������� ������: ";
	cin >> e;
	if(find_node(tree, e))
	{
		cout << endl << "����� ������� � � �����!" << endl << endl;
		cout<<"г���� - ";
		findAmountOfParents(tree, e, -1, len);
		cout << len;
	}
	else
		cout << endl << "������ �������� ���� � �����!" << endl <<
		     endl;
	del(tree);//������� ���'�� ������� �� ������
	return 0;
}

